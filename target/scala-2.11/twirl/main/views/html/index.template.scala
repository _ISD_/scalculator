
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object index_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

class index extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.4*/("""

"""),format.raw/*3.1*/("""<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Scalculator</title>
</head>
<body>
    <h1>The Scalculator project</h1>
    <p>Scalculator: GET <a href="/calculus?query=">/calculus?query=BASE64FORMATTEDQUERY</a> to calculate something</p>
    <h2>Example queries</h2>
    <p>
        To encode calculus queries to base64 you can check
        <a href="https://www.base64encode.org/">base64encode.org</a>.<br />
        Here are some examples:<br/>
        <ul>
        <li>(1138 * (46/18)) / (2*2*2*2*2) -> <a href="/calculus?query=KDExMzggKiAoNDYvMTgpKSAvICgyKjIqMioyKjIp">KDExMzggKiAoNDYvMTgpKSAvICgyKjIqMioyKjIp</a></li>
        <li>(5*4*3*2*1)/5 -> <a href="/calculus?query=KDUqNCozKjIqMSkvNQ==">KDUqNCozKjIqMSkvNQ==</a></li>
        <li>(4 * (64 / (3+128))- 23 * (2*8))-8/4 -> <a href="/calculus?query=KDQgKiAoNjQgLyAoMysxMjgpKS0gMjMgKiAoMio4KSktOC80">KDQgKiAoNjQgLyAoMysxMjgpKS0gMjMgKiAoMio4KSktOC80</a></li>
    </ul>
    </p>
</body>
</html>"""))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


}

/**/
object index extends index_Scope0.index
              /*
                  -- GENERATED --
                  DATE: Sat Apr 08 20:14:56 EEST 2017
                  SOURCE: D:/Scala/scalculator/app/views/index.scala.html
                  HASH: 62932ef9bf91300718616f6bed0e3f7f5d7f45ea
                  MATRIX: 520->1|616->3|646->7
                  LINES: 20->1|25->1|27->3
                  -- GENERATED --
              */
          