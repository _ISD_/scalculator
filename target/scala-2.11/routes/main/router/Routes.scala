
// @GENERATOR:play-routes-compiler
// @SOURCE:D:/Scala/scalculator/conf/routes
// @DATE:Sat Apr 08 20:21:52 EEST 2017

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._
import play.core.j._

import play.api.mvc._

import _root_.controllers.Assets.Asset

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:2
  RootController_0: controllers.RootController,
  // @LINE:7
  Assets_1: controllers.Assets,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:2
    RootController_0: controllers.RootController,
    // @LINE:7
    Assets_1: controllers.Assets
  ) = this(errorHandler, RootController_0, Assets_1, "/")

  import ReverseRouteContext.empty

  def withPrefix(prefix: String): Routes = {
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, RootController_0, Assets_1, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """calculus""", """controllers.RootController.calculate(query:String)"""),
    ("""GET""", this.prefix, """controllers.RootController.index"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """assets/""" + "$" + """file<.+>""", """controllers.Assets.at(path:String = "/public", file:String)"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:2
  private[this] lazy val controllers_RootController_calculate0_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("calculus")))
  )
  private[this] lazy val controllers_RootController_calculate0_invoker = createInvoker(
    RootController_0.calculate(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.RootController",
      "calculate",
      Seq(classOf[String]),
      "GET",
      """ /calculus?query=base64encodedquerystring""",
      this.prefix + """calculus"""
    )
  )

  // @LINE:4
  private[this] lazy val controllers_RootController_index1_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix)))
  )
  private[this] lazy val controllers_RootController_index1_invoker = createInvoker(
    RootController_0.index,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.RootController",
      "index",
      Nil,
      "GET",
      """""",
      this.prefix + """"""
    )
  )

  // @LINE:7
  private[this] lazy val controllers_Assets_at2_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("assets/"), DynamicPart("file", """.+""",false)))
  )
  private[this] lazy val controllers_Assets_at2_invoker = createInvoker(
    Assets_1.at(fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Assets",
      "at",
      Seq(classOf[String], classOf[String]),
      "GET",
      """ Map static resources from the /public folder to the /assets URL path""",
      this.prefix + """assets/""" + "$" + """file<.+>"""
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:2
    case controllers_RootController_calculate0_route(params) =>
      call(params.fromQuery[String]("query", None)) { (query) =>
        controllers_RootController_calculate0_invoker.call(RootController_0.calculate(query))
      }
  
    // @LINE:4
    case controllers_RootController_index1_route(params) =>
      call { 
        controllers_RootController_index1_invoker.call(RootController_0.index)
      }
  
    // @LINE:7
    case controllers_Assets_at2_route(params) =>
      call(Param[String]("path", Right("/public")), params.fromPath[String]("file", None)) { (path, file) =>
        controllers_Assets_at2_invoker.call(Assets_1.at(path, file))
      }
  }
}
