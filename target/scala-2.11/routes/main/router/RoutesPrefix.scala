
// @GENERATOR:play-routes-compiler
// @SOURCE:D:/Scala/scalculator/conf/routes
// @DATE:Sat Apr 08 20:21:52 EEST 2017


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
