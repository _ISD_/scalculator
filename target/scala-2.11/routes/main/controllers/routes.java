
// @GENERATOR:play-routes-compiler
// @SOURCE:D:/Scala/scalculator/conf/routes
// @DATE:Sat Apr 08 20:21:52 EEST 2017

package controllers;

import router.RoutesPrefix;

public class routes {
  
  public static final controllers.ReverseRootController RootController = new controllers.ReverseRootController(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseAssets Assets = new controllers.ReverseAssets(RoutesPrefix.byNamePrefix());

  public static class javascript {
    
    public static final controllers.javascript.ReverseRootController RootController = new controllers.javascript.ReverseRootController(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseAssets Assets = new controllers.javascript.ReverseAssets(RoutesPrefix.byNamePrefix());
  }

}
