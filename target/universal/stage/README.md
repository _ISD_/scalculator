# Scalculator 

A miniproject to implement a calculator web service

## Technology used

* Scala 2.12
* SBT 0.13.13.1
* Play framework 2.5

## Usage
* .../calculus?query=QUERYSTRING
* Where the query is a base64 encoded string
