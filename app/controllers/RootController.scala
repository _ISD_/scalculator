package controllers

import javax.inject.Inject

import app.scalculator.CalculatorAction
import play.api.libs.json.JsValue
import play.api.mvc.{Action, Controller}


class RootController @Inject()(action: CalculatorAction) extends Controller {

  // Render the index template with some usage information
  def index = Action {
    Ok(views.html.index())
  }

  // Just pass on the encoded query string to the CalculatorAction, returns json
  def calculate(encodedQuery: String) = Action {
    Ok(action.decodeAndCalculate(encodedQuery): JsValue)
  }

}