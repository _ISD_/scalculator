package app.scalculator

import org.apache.commons.codec.binary.StringUtils
import org.apache.commons.codec.binary.Base64
import play.api.mvc._
import play.api.Logger
import play.api.libs.json.{JsValue, Json}

import scala.collection.mutable
import scala.concurrent.Future

class CalculatorAction extends ActionBuilder[Request] {

  override def invokeBlock[A](
                  request: Request[A],
                  block: Request[A] => Future[Result]): Future[Result] = {

    Logger.debug(s"HTTP request: ${request.method} ${request.uri}")
    block(request)
  }

  // Decodes and validates the query, then converts it to postfix and finally calculates the result
  def decodeAndCalculate(encodedQuery: String): JsValue = {
    var result = Json.obj()

    try {
      result = Json.obj("error" -> false, "result" -> calculate(convertToPostfix(decodeAndValidate(encodedQuery))))
    } catch {
      // Catch any thrown Exceptions and pass the message to the user
      case e: Throwable => result = Json.obj("error" -> true, "message" -> e.getMessage)
    }
    result
  }

  // Decodes the Base64 String to human readable format and validates that it only contains allowed characters
  def decodeAndValidate(encodedQuery: String): String = {
    // If there's no input, return a straightforward error message
    if (encodedQuery.length == 0) {
      throw new RuntimeException("No input")
    }
    val decodedString = StringUtils.newStringUtf8(Base64.decodeBase64(encodedQuery))

    // Pattern: whitespace, numbers, plus, minus, slash, asterisk and parenthesis signs
    val pattern = "[\\s\\d\\+\\-\\/\\*\\(\\)]+"

    // If the query contained any illegal characters, throw an exception with the problematic query in the message
    if (!decodedString.matches(pattern)) {
      throw new RuntimeException("Invalid calculus query: "+ decodedString)
    }

    decodedString
  }

  // Calculates the postfix formatted query string
  def calculate(calculusQuery: String): Double = {

    // To get the strangely formatted string into order remove the " !" spacers between the symbols of the same number,
    // then split the string by the blank spaces into tokens
    val queryArray = calculusQuery.replaceAll("\\s!", "").split(" ")

    var result: Double = 0D
    val stack = new mutable.Stack[String]

    for (i <- 0 until queryArray.length) {
      val token = queryArray(i)

      if (token.matches("[\\d]+")) {
        stack.push(token)

      } else {
        // All the supported operations require two numbers
        if (stack.length < 2) {
          throw new RuntimeException("Insufficient values in the input")
        } else {
          // Pop two values and evaluate the calculation, then push the result back into the stack
          stack.push(evaluate(stack.pop(), stack.pop(), token).toString)
        }
      }
    }

    if (stack.length.equals(1)) {
      result = stack.pop().toDouble
    } else {
      throw new RuntimeException("The user input has too many values")
    }
    result
  }

  def evaluate(valueA: String, valueB: String, operator: String): Double = {
    var result: Double = 0D
    try{
      // There's way too much repetition here but...
      operator match {
        case "+" => result = valueA.toDouble + valueB.toDouble
        case "-" => result = valueB.toDouble - valueA.toDouble
        case "*" => result = valueA.toDouble * valueB.toDouble
        case "/" => result = valueB.toDouble / valueA.toDouble
      }
    } catch {
      case e:Throwable => throw new RuntimeException(e.getMessage)
    }
    result
  }

  // An attempt at Dijkstra's shunting yard algorithm for converting the query to postfix
  def convertToPostfix(calculusQuery: String): String = {
    val numeric = "[\\d]"
    val leftParen = "[\\(]"
    val rightParen = "[\\)]"
    val multiplication = "[\\*]"
    val division = "[\\/]"
    val sum = "[\\+]"
    val subtraction = "[\\-]"
    val operators = sum +"||"+ subtraction +"||"+ multiplication +"||"+ division
    val operatorStack = new mutable.Stack[String]
    val output = new mutable.Queue[String]

    var lastToken = ""
    for (char <- calculusQuery) {
      val token = char.toString()

      if (token.matches(numeric)) {
        // A silly hack to check if the last token was numeric and this is an nth part of a multichar
        // number (10, 100 etc)
        if (output.nonEmpty && lastToken != "" && lastToken.matches(numeric)) {
          // Add a ! to keep track of the multicharacter numbers instead of splitting them all up
          output += "!"+token
        } else {
          output += token
        }
      }

      if (token.matches(operators)) {
        // Pop operators out of the stack if they are worth less than the stack's top operator (adding and subtracting
        // are done after multiplication and division
        while (operatorStack.nonEmpty && operatorStack.top.matches(operators) && presedenceIsLowerOrEqual(token, operatorStack.top)) {
          output += operatorStack.pop()
        }
        operatorStack.push(token)
      }

      if (token.matches(leftParen)) {
        operatorStack.push(token)
      }

      if (token.matches(rightParen)) {
        while (!operatorStack.top.matches(leftParen)) {
          // Until a closing parenthesis is found, pop operators to the output queue
          output += operatorStack.pop()
        }
        // Pop the right parenthesis out
        operatorStack.pop()
      }
      // To keep track of multichar numbers, remember the last token processed
      lastToken = token
    }

    while (operatorStack.nonEmpty) {
      if (operatorStack.top.matches(leftParen)) {
        throw new RuntimeException("Mismatched parenthesis")
      }
      // Pop the rest of the operators to the output
      output += operatorStack.pop()
    }

    // Separate each token with a space
    output.mkString(" ")
  }

  // Compare the operator's importance against another operator
  def presedenceIsLowerOrEqual(firstOperator: String, secondOperator: String): Boolean = {
    val tokenPresedence: Int = getTokenPresedence(firstOperator)
    val otherTokenPresedence: Int = getTokenPresedence(secondOperator)

    // If a token's presedence <= operator stack's top operator's presedence
    (tokenPresedence compare otherTokenPresedence).signum match {
      case 1 => return false
      case 0 => return true
      case -1 => return true
    }
  }

  def getTokenPresedence(operator: String): Int = {
    // numbers have the lowest presedence
    var presedence = 1
    operator match {
      case "*" => presedence = 3
      case "/" => presedence = 3
      case "+" => presedence = 2
      case "-" => presedence = 2
      case _ => presedence = 1
    }
    presedence
  }
}
