import org.scalatest.FreeSpec
import org.scalatestplus.play.OneServerPerSuite
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.libs.ws.WSClient

import scala.concurrent.Await
import scala.concurrent.duration.Duration


class CalculatorIntegration extends FreeSpec with OneServerPerSuite {


  //implicit override lazy val app = new GuiceApplicationBuilder().build()


  "Server" - {
    "When requested for the index" - {
      "should return it" in {
          val ws = app.injector.instanceOf[WSClient]
          val response = ws.url(s"http://localhost:$port/").get()
          val result = Await.result(response, Duration.Inf)
          assert(result.status === 200)
        }
      }
    }
    "When requesting a calculation with an empty string" - {
      "should return json with a 'No input' error message" in {
        val ws = app.injector.instanceOf[WSClient]
        val response = ws.url(s"http://localhost:$port/calculus?query=").get()
        val result = Await.result(response, Duration.Inf)
        val errorStatus = Json.parse(result.body) \ "error"
        val message = Json.parse(result.body) \ "message"
        assert(result.status === 200)
        //assert(errorStatus.toString.equals("true"))
        //assert(message.toString.equals("No input"))
      }
    }
    "When requesting a calculation with an encoded 1+1 string" - {
      "should return json with a result: 2 message" in {
        val ws = app.injector.instanceOf[WSClient]
        val response = ws.url(s"http://localhost:$port/calculus?query=MSsx").get()
        val result = Await.result(response, Duration.Inf)
        val errorStatus = Json.parse(result.body) \ "error"
        val message = Json.parse(result.body) \ "result"
        assert(result.status === 200)
        //assert(errorStatus.toString.equals("false"))
        //assert(message.toString.equals("2"))
      }
    }
    "When requesting a calculation with an encoded ((1+1) string" - {
      "should return json with a 'Mismatched parenthesis' error message" in {
        val ws = app.injector.instanceOf[WSClient]
        val response = ws.url(s"http://localhost:$port/calculus?query=KCgxKzEp").get()
        val result = Await.result(response, Duration.Inf)
        val errorStatus = Json.parse(result.body) \ "error"
        val message = Json.parse(result.body) \ "message"
        assert(result.status === 200)
        //assert(errorStatus.toString.equals("false"))
        //assert(message.toString.equals("Mismatched parenthesis"))
      }
    }
    "When requesting a calculation with an encoded a+b*c string" - {
      "should return json with a 'Mismatched parenthesis' error message" in {
        val ws = app.injector.instanceOf[WSClient]
        val response = ws.url(s"http://localhost:$port/calculus?query=YStiKmM=").get()
        val result = Await.result(response, Duration.Inf)
        val errorStatus = Json.parse(result.body) \ "error"
        val message = Json.parse(result.body) \ "message"
        assert(result.status === 200)
        //assert(errorStatus.toString.equals("false"))
        //assert(message.toString.equals("Invalid calculus query: a+b*c"))
      }
    }
  }